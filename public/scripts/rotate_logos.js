var rotateLogos = function InitRotateLogos (){
    function init(){
        //Read extra info
        var extraInfoCanvas = document.getElementById("epyx_extraInfo");
        var extraInfoContent =  extraInfoCanvas.innerHTML;
        var extraInfoData = JSON.parse(extraInfoContent);
        //Left Logos ( FAI, FAI_CIVL, APWT + Sponsors)
        var oldRotateLeftLogosInterval = sessionStorage.getItem("rotateLeftLogosInterval");
        if (oldRotateLeftLogosInterval) {
            window.clearInterval(oldRotateLeftLogosInterval);
        }

        var leftLogoList = extraInfoData.leftLogos;
        if(leftLogoList){
            if(leftLogoList.length === 1){
                document.getElementById("logo_box").style.backgroundImage = 'url(' + leftLogoList[0].imageSrc + ')';
            }

            if(leftLogoList.length > 1){
                var leftLogoIndex = 0;
                var cantLeftLogos = leftLogoList.length;
                document.getElementById("logo_box").style.backgroundImage = 'url(' + leftLogoList[0].imageSrc + ')';
                function RotateLeftLogos(){
                    leftLogoIndex++;
                    if (leftLogoIndex === cantLeftLogos) {
                        leftLogoIndex = 0;
                    }
                    document.getElementById("logo_box").style.backgroundImage = 'url(' + leftLogoList[leftLogoIndex].imageSrc + ')';
                }
                var rotateLeftLogosInterval = window.setInterval(function(){
                    RotateLeftLogos();
                }, 45000);
                sessionStorage.setItem("rotateLeftLogosInterval", rotateLeftLogosInterval);
            }
        }

//Right Logos (Event Logo + Sponsors)
        var oldRotateRightLogosInterval = sessionStorage.getItem("rotateRightLogosInterval");
        if (oldRotateRightLogosInterval) {
            window.clearInterval(oldRotateRightLogosInterval);
        }

        var rightLogoList = extraInfoData.rightLogos;
        if(rightLogoList){
            if(rightLogoList.length === 1){
                document.getElementById("logo_competition_box").style.backgroundImage = 'url(' + rightLogoList[0].imageSrc + ')';
            }

            if(rightLogoList.length > 1){
                var rightLogoIndex = -2;
                var cantRightLogos = rightLogoList.length;
                document.getElementById("logo_competition_box").style.backgroundImage = 'url(' + rightLogoList[0].imageSrc + ')';
                function RotateRightLogos(){
                    rightLogoIndex++;
                    if (rightLogoIndex === cantRightLogos) {
                        rightLogoIndex = -2;
                    }

                    if(rightLogoIndex === -2){
                        document.getElementById("logo_competition_box").style.backgroundImage = 'url(' + rightLogoList[0].imageSrc + ')';
                    }

                    if(rightLogoIndex > 0) {
                        document.getElementById("logo_competition_box").style.backgroundImage = 'url(' + rightLogoList[rightLogoIndex].imageSrc + ')';

                    }
                }
                var rotateRightLogosInterval = window.setInterval(function(){
                    RotateRightLogos();
                }, 10000);

                sessionStorage.setItem("rotateRightLogosInterval", rotateRightLogosInterval);
            }
        }
    }

    return {
        start: function(){
            init();
        }
    };

}();

//window.addEventListener('load', rotateLogos.start, false);







