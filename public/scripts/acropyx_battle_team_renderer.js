if ( epyxRenderer ) {
	epyxRenderer.registerRenderer("epyx_battle_team_canvas", function(context,w,h,data) {
                    
		var x = 0;
		var y = 0;
		
		var headSize = h/6.5;
		
		//----- Display base
		context.save();
		var my_gradient = context.createLinearGradient(x, y, x, y+h);
		my_gradient.addColorStop(0, "#CCC");
		my_gradient.addColorStop(1, "#EEE");
		context.fillStyle = my_gradient;
		context.strokeStyle = "rgb(0,0,0)";
		
		context.shadowOffsetX = 0;
		context.shadowOffsetY = 3;
		context.shadowColor = "rgba(0,0,0,0.3)";
		context.shadowBlur = 4;
		epyxRenderer.roundRect(context,x+1, y+1, w-2, h-8,20,true,true);

        var iw = 0
        //----- Display Team Image (Logo)
        var flag = new Image();
        flag.src = "images/Team-128.png";
        flag.onload = function() {
            var posX = 0;
            var posY = 0;
            var ratio = this.width / this.height;
            var ih = headSize*2.5;
            iw = ih * ratio;
            context.save();
            context.shadowOffsetX = 1;
            context.shadowOffsetY = 3;
            context.shadowColor = "rgba(0,0,0,0.3)";
            context.shadowBlur = 7;
            //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
            context.drawImage(this,posX,posY,iw,ih);

            context.restore();
        };
		//----- Display Team name
		context.font = "bold "+headSize+"pt arial";
		context.fillStyle = "black";
		context.fillText(data.name,x+2.5*headSize,y+headSize*1.4);
        context.restore();



        if ( data.pilots[0].country ) {
            var flag = new Image();
            flag.src = "images/flags/"+data.pilots[0].country.toLowerCase()+".png";
            flag.onload = function() {
                var posX = 2;
                var posY = y + headSize*2.3 ;
                var ratio = this.width / this.height;
                var ih = headSize * 1.5 ;
                var iw = ih * ratio;
                context.save();
                context.shadowOffsetX = 1;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 7;
                //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                context.drawImage(this,posX,posY,iw,ih);
                context.font = "bold "+headSize/1.2+"pt arial";
                context.fillStyle = "black";
                context.fillText(data.pilots[0].name,x+iw+10,y+headSize*3.6);
                context.restore();
            };
        } else{
            context.save();
            context.font = "bold "+headSize+"pt arial";
            context.fillStyle = "black";
            context.fillText(data.pilots[0].name,x+iw+10,y+headSize*3.6);
            context.restore();

        }

        context.restore();

        if ( data.pilots[1].country ) {
            var flag = new Image();
            flag.src = "images/flags/"+data.pilots[1].country.toLowerCase()+".png";
            flag.onload = function() {
                var posX = 2;
                var posY = y + headSize*4 ;
                var ratio = this.width / this.height;
                var ih = headSize * 1.5 ;
                var iw = ih * ratio;
                context.save();
                context.shadowOffsetX = 1;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 7;
                //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                context.drawImage(this,posX,posY,iw,ih);
                context.font = "bold "+headSize/1.2+"pt arial";
                context.fillStyle = "black";
                context.fillText(data.pilots[1].name,x+iw+10,y+headSize*5.2);
                context.restore();
            };
        } else{
            context.save();
            context.font = "bold "+headSize+"pt arial";
            context.fillStyle = "black";
            context.fillText(data.pilots[0].name,x+iw+10,y+headSize*3.6);
            context.restore();

        }
        var posX = x+w
        if ( data.pilots[0].imageSrc ) {
            var photo = new Image();
            photo.src = data.pilots[0].imageSrc;
            photo.onload = function() {
                var ih = h-headSize*2.3;
                var posY = headSize*2;
                var ratio = this.width / this.height;
                var iw = ih * ratio;
                posX =  posX - iw - 8;
                context.save();

                epyxRenderer.roundedCorners(context,posX,posY,iw,ih,20,1,1,1,1,false);
                context.clip();
                context.fillStyle = "#EEE";
                context.fillRect(posX,posY,iw,ih);
                context.drawImage(this,posX,posY,iw,ih);
                context.restore();

                context.save();
                context.shadowOffsetX = 0;
                context.shadowOffsetY = 0;
                context.shadowColor = "rgba(0,0,0,0.8)";
                context.shadowBlur = 4;
                context.strokeStyle = "white";
                epyxRenderer.roundedCorners(context,posX,posY,iw,ih,20,1,1,1,1,false);
                context.lineWidth = 2;
                context.stroke();
                context.restore();
            }
        }

        if ( data.pilots[1].imageSrc ) {
            var photo = new Image();
            photo.src = data.pilots[1].imageSrc;
            photo.onload = function() {
                var ih = h-headSize*2.3;
                var posY = headSize*2;
                var ratio = this.width / this.height;
                var iw = ih * ratio;
                posX = posX - iw - 8;
                context.save();

                epyxRenderer.roundedCorners(context,posX,posY,iw,ih,20,1,1,1,1,false);
                context.clip();
                context.fillStyle = "#EEE";
                context.fillRect(posX,posY,iw,ih);
                context.drawImage(this,posX,posY,iw,ih);
                context.restore();

                context.save();
                context.shadowOffsetX = 0;
                context.shadowOffsetY = 0;
                context.shadowColor = "rgba(0,0,0,0.8)";
                context.shadowBlur = 4;
                context.strokeStyle = "white";
                epyxRenderer.roundedCorners(context,posX,posY,iw,ih,20,1,1,1,1,false);
                context.lineWidth = 2;
                context.stroke();
                context.restore();
            }
        }




		//----- Global highlight
		context.save();
		
		context.shadowOffsetX = 0;
		context.shadowOffsetY = -15;
		context.shadowColor = "rgba(255,255,255,0.8)";
		context.shadowBlur = 5;
		var my_gradient = context.createLinearGradient(x, y+headSize*1.5, x, y+headSize*1.5 + (headSize /2) );
		my_gradient.addColorStop(0, "rgba(0,0,0,0.0)");
		my_gradient.addColorStop(1, "rgba(0,0,0,0.2)");
		context.fillStyle = my_gradient;
		epyxRenderer.roundRect(context,x+1, y+headSize*1.5, w-10,(headSize /2),2,true,false);
		//context.fillRect(x+1,y+1,w-2,50);
		context.restore();


		
	});	
} else {
	if ( window.console ) {
		window.console.error("No epyxRenderer definied, you should import the script epyx_rendere.js before using this one");
	}
}
