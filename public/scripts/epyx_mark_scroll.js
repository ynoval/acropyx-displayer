var epyxMarkTableScroll = function() {
	var toScroll;
	var y = 0;
	var dyDown = -1;
	var dyUp = 5;

	var speed = 50;
	var maxHeight;

	var offsetWait = 150;

	var rowHeight = 70;
	var fontStyle = "bold 24pt arial,sans-serif";
    var fontPilotSyncStyle = "bold 24pt arial,sans-serif";


	var jsonContent;
	
	var logDiv;
	
	function log(str) {
		if ( window.console ) {
			window.console.log( str );
		}
		else if (document.getElementById("log_div")) {
			document.getElementById("log_div").innerHTML += str + "<br>";
		}
	}

	function draw() {
		if (!toScroll) {
			return;
		}
		y += dy;
		if (y <= -(maxHeight + Math.round(offsetWait/4))) {
			dy = dyUp;
		}
		if (y >= offsetWait) {
			dy = dyDown;
		}
		// alert("draw-3 y= "+y);
		if ((y >= -maxHeight) && (y <= 0)) {
			toScroll.style.top = y + "px";
		}
	}

	/**
	 * Double buffering draw of the table
	 */
	function paint(canvas) {
		var list = getData(canvas);

		if(list) {
            //Check sync
            var syncHeight = 1;
            if (list[0].pilot1 != null) {
                syncHeight = 2 //* rowHeight;
            }


            canvas.width = canvas.clientWidth;
            canvas.height = syncHeight * rowHeight * list.length + 10;
            /*
             * Do not use double buffering when using images...
             var bufferContext = buffer.getContext("2d");
             // draw in buffer context then switch
             paintContent(bufferContext, canvas.width, list);
             var data = bufferContext.getImageData(0, 0, buffer.width, buffer.height);
             canvas.getContext("2d").putImageData(data, 0, 0);
             */
            paintTable(canvas.getContext("2d"), 0, 5, canvas.width, list);
        }
	}

	function getData( canvas ) {
		var content = jsonContent;
		if ( !content ) {
			content = canvas.innerHTML;
		}
		return JSON.parse(content);
	}
	
	/**
	 * Paint table content in the given context
	 * height is calculated by rowHeight
	 */
	function paintTable(context,px,py,w, list) {
		var y = py;
        var syncHeight = 1
        if (list[0].pilot1 != null )
        {
            syncHeight = 1.5; // * rowHeight;
        }
        var h = syncHeight * rowHeight;

        if (list[0].competitor1 != null){
            for ( var i = 0; i < list.length; i++) {
                paintRowBattle(context, px, y+5, h, w-2, list[i],i+1);
                y += h;
            }
        } else {
            var pos = 0;
            var lastMark = -1;
            var lastPos = 0;
            for ( var i = 0; i < list.length; i++) {
                //Check equal marks
                if(list[i].mark && list[i].mark === lastMark){
                    pos = lastPos;
                } else {
                    pos = i +1;
                    lastPos = i+1;
                    lastMark = list[i].mark;
                }

                paintRow(context, px, y+5, h, w-2, list[i],pos);

                y += h;
            }
        }

	}

	function getCountryImageSrc( countryCode ) {
		return "images/flags/"+ countryCode.toLowerCase()+".png";
	}

    function getWarningsImageSrc( obj ) {
        if (obj.warnings >= 3)
        {
            return  "images/red_card.png";
        }

        return "images/yellow_card.png";
    }


    function getClassifiedStatusImageSrc(status){
        switch (status){
            case 0: {
                return "images/icons/eliminated_2.png";
            }
            case 1: {
                return "images/icons/check_2.png";
            }
            case 2:{
                return "images/icons/lucky_2.png";
            }
            default:{
                return "images/icons/check_2.png";
            }
        }
    }

    function getQualifyStatusImageSrc(status){
        switch (status){
            case -1: {
                return "images/icons/eliminated_2.png";
            }
            case 0:{
                return "images/icons/qualify.png";
            }
            case 1: {
                return "images/icons/qualify_lucky.png";
            }
            case 2:{
                return "images/icons/qualify_lucky.png";
            }
        }
    }
	/**
	 * paint the given row in a context and at position
	 */
	function paintRow(context, x, y, h, w, row, position) {
		context.save();

        var syncHeight  = 1;
        if (row.pilot1 != null)
        {
            syncHeight  = 1.5;
        }

		var my_gradient = context.createLinearGradient(x, y, x, y+h );
		my_gradient.addColorStop(0, "#CCC");
		my_gradient.addColorStop(1, "#EEE");
		context.fillStyle = my_gradient;
		context.strokeStyle = "rgb(0,0,0)";
		
		context.shadowOffsetX = 0;
		context.shadowOffsetY = 3;
		context.shadowColor = "rgba(0,0,0,0.3)";
		context.shadowBlur = 4;
		roundRect(context,x+1, y+1, w-2, h -8,15,true,true);
		
		context.restore();

		var textLineY = y + h/syncHeight - 22;
		var mark_width = 0;
		if (row.mark || row.mark == 0){
			mark_width = 130
		}
		var flag_width = 0;
		if (row.country){
			flag_width = 60
		}
        var flag_width_sync = 0;
        if (row.country1){
            flag_width_sync = 60
        }


        var nbRuns_width = 0;
        if (row.nbRuns > 0)
        {
            nbRuns_width = 150;
        }

        var quality_width = 60;
//        if(row.qualifyStatus){
//          quality_width = 60;
//        }


		//---------- draw number ----------------
		context.save();
		var nb_width = 60;
		my_gradient = context.createLinearGradient(x, y+5, x, y+h/syncHeight-10);
		my_gradient.addColorStop(0, "#888");
		my_gradient.addColorStop(1, "#333");
		context.fillStyle = my_gradient;
		context.drawWidth=2;
		roundRect(context,x+5, y+5, nb_width, h/syncHeight-16,15,true,true);
		context.fillStyle = "white";
		context.font = fontStyle;
		context.textAlign = "right";
		context.fillText(""+position, nb_width-2, textLineY);
		
		//+++++-- Highlight ---
		my_gradient = context.createLinearGradient(x, y, x, y+30);
		my_gradient.addColorStop(0, "rgba(255,255,255,0.0)");
		my_gradient.addColorStop(1, "rgba(255,255,255,0.3)");
		context.fillStyle = my_gradient;
		roundRect(context,x+7, y+8, nb_width-12, 15,10,true,false);
		context.restore();

		
		//-------- draw competitor text -------------
		context.save();
		var my_gradient = context.createLinearGradient(x, y, x, y+20);
		my_gradient.addColorStop(0, "rgba(255,255,255,0.8)");
		my_gradient.addColorStop(1, "rgba(255,255,255,0.0)");
		context.fillStyle = my_gradient;
	    	var endCompetitorGradientX = w-mark_width-80;
			
		roundRect(context, x+nb_width+10, y+5 , endCompetitorGradientX, 40, 20, true, false);
		
		context.fillStyle = "#333";
		context.textAlign = "left";
		context.font = fontStyle;

        context.fillText(row.name, x+nb_width+flag_width+14, textLineY);

        //Paint warnings
        if ( row.warnings > 0 ) {
            var cardImage = new Image();
            cardImage.src = getWarningsImageSrc(row);

            cardImage.onload = function() {
                context.save();
                context.shadowOffsetX = 3;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 5;
                //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                context.drawImage(cardImage,endCompetitorGradientX- nbRuns_width - quality_width,y+6,rowHeight-17,rowHeight-17);

                if (row.warnings == 2)
                {
                    context.drawImage(cardImage,endCompetitorGradientX-nbRuns_width- quality_width -40 ,y+6,rowHeight-17,rowHeight-17);
                }

                context.restore();
            };
        }

        //Print Runs count
        if (row.nbRuns) {
            var runText = (row.nbRuns == 1)? " run" : " runs";
            context.fillText(row.nbRuns + runText , endCompetitorGradientX - quality_width - 70, textLineY );
        }
        context.restore();



        if (row.pilot1 != null)
        {
            if ( row.country1 ) {
                var flag = new Image();
                flag.src = getCountryImageSrc(row.country1);
                flag.onload = function() {
                    var posX = -mark_width-60;

                    context.save();
                    context.shadowOffsetX = 3;
                    context.shadowOffsetY = 3;
                    context.shadowColor = "rgba(0,0,0,0.3)";
                    context.shadowBlur = 5;
                    //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                    context.drawImage(flag,x+nb_width+12 +430,y +6,rowHeight-25,rowHeight-25);

                    context.restore();
                };
            }

            context.fillStyle = "#333";
            context.textAlign = "left";
            context.font = fontPilotSyncStyle;

            context.fillText(row.pilot1, x+nb_width+flag_width_sync+430, textLineY -2 );

            if ( row.country2 ) {
                var flag2 = new Image();
                flag2.src = getCountryImageSrc(row.country2);
                flag2.onload = function() {
                    var posX = -mark_width-60;

                    context.save();
                    context.shadowOffsetX = 3;
                    context.shadowOffsetY = 3;
                    context.shadowColor = "rgba(0,0,0,0.3)";
                    context.shadowBlur = 5;
                    context.drawImage(flag2,x+nb_width+12 + 430,y+ 40 + 10,rowHeight-25,rowHeight-25);
                    context.restore();
                };
            }

            context.fillText(row.pilot2, x+nb_width+flag_width_sync + 430, textLineY -2 + 40);
        }


		//---------- draw flag ------------
		if ( row.country ) {
			var flag = new Image();
			flag.src = getCountryImageSrc(row.country);
			flag.onload = function() {
				var posX = -mark_width-60;
	
				context.save();
				context.shadowOffsetX = 3;
				context.shadowOffsetY = 3;
				context.shadowColor = "rgba(0,0,0,0.3)";
				context.shadowBlur = 5;
				//context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
				context.drawImage(flag,x+nb_width+12,y+6,rowHeight-17,rowHeight-17);
				
				context.restore();
			};
		}

        //---------- draw status ------------
        if (row.qualifyStatus != null){
            var classifiedImage = new Image();
            classifiedImage.src = getQualifyStatusImageSrc(row.qualifyStatus);
            classifiedImage.onload = function() {
                var posX = endCompetitorGradientX-5; // TODO: create getClassifiedImageWidth
                var posY = y + 7;
                var ratio = this.width / this.height;
                var ih = rowHeight - 17;
                var iw = ih * ratio;
                context.save();
                context.shadowOffsetX = 1;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 7;
                context.drawImage(this,posX,posY,iw,ih);

                context.restore();
            };
        }

		//------- draw mark ---------------------------
		context.save();
		
		
		if ( row.mark || row.mark == 0) {
		var fontColor = "white";
		var glow = undefined;
		my_gradient = context.createLinearGradient(x, y+5, x, y+h/syncHeight-10);
		if ( position == 1 ) {
			// gold
			my_gradient.addColorStop(0, "#A95");
			my_gradient.addColorStop(1, "#A92");
			fontColor = "white";
			glow = "yellow";
		} else if ( position == 2 ) {
			// silver
			my_gradient.addColorStop(0, "#BBB");
			my_gradient.addColorStop(1, "#666");
			fontColor = "white";
			glow = "#CCC";
		} else if ( position == 3 ) {
			// bronze
			my_gradient.addColorStop(0, "#B65");
			my_gradient.addColorStop(1, "#B62");
			fontColor = "white";
			glow = "orange";
		} else {
			my_gradient.addColorStop(0, "#DDD");
			my_gradient.addColorStop(1, "#CCC");
			fontColor = "#444";
		}
		context.fillStyle = my_gradient;
		roundRect(context,x+w-mark_width-5, y+5, mark_width, h/syncHeight-16,15,true,true);
		
		if ( glow ) {
			context.shadowOffsetX = 0;
			context.shadowOffsetY = 0;
			context.shadowColor = glow;
			context.shadowBlur = 10;
		}
		context.fillStyle = fontColor;
		context.font = "bold 30pt arial";
		context.textAlign = "right";
    var markText = (row.mark < 0)?'DSQ':row.mark;
    context.fillText(markText, x+w-10, textLineY);
		context.restore();
		//+++++-- Highlight ---
		my_gradient = context.createLinearGradient(x, y, x, y+30);
		my_gradient.addColorStop(0, "rgba(255,255,255,0.0)");
		my_gradient.addColorStop(1, "rgba(255,255,255,0.4)");
		context.fillStyle = my_gradient;
		roundRect(context,x+w-mark_width-3, y+8, mark_width-6, 15,10,true,false);
		}
	}

  function paintRowBattle(context, x, y, h, w, row, position) {
        context.save();

        //Constants

        var nb_width = 60;
        var middle = (w - nb_width)/2;
        var flag_width_margin = 12;

        var flag_icon_height = 256;
        var flag_icon_radio = 1;
        var flag_width = (rowHeight  - 17) / flag_icon_radio;

        var vs_icon_height = 709;
        var vs_icon_radio = 0.7;
        var vs_width = (rowHeight  -17) / vs_icon_radio;

        //TODO: Get right width with radio ...
        var classifiedCheck_width = 80;
        var classifiedCancel_width = 80;
        var classifiedLucky_width = 80;



        var syncHeight  = 1;
        //Check if the row draw a team
        if (row.competitor1.pilot1 != null)
        {
            syncHeight  = 1.5;
        }

        //TODO: check gradient  maybe  y * syncHeight!!
        var my_gradient = context.createLinearGradient(x, y, x, y+h );
        my_gradient.addColorStop(0, "#CCC");
        my_gradient.addColorStop(1, "#EEE");
        context.fillStyle = my_gradient;
        context.strokeStyle = "rgb(0,0,0)";

        context.shadowOffsetX = 0;
        context.shadowOffsetY = 3;
        context.shadowColor = "rgba(0,0,0,0.3)";
        context.shadowBlur = 4;
        roundRect(context,x+1, y+1, w-2, h -8,15,true,true);

        context.restore();

        var textLineY = y + h/syncHeight - 22;
        var mark_width = 0;
        //TODO: CHECK!!!!!
        if (row.competitor1.mark || row.competitor1.mark == 0){
            mark_width = 130
        }

        //---------- draw number ----------------
        context.save();

        my_gradient = context.createLinearGradient(x, y+5, x, y+h/syncHeight-10);
        my_gradient.addColorStop(0, "#888");
        my_gradient.addColorStop(1, "#333");
        context.fillStyle = my_gradient;
        context.drawWidth=2;
        roundRect(context,x+5, y+5, nb_width, h/syncHeight-16,15,true,true);
        context.fillStyle = "white";
        context.font = fontStyle;
        context.textAlign = "right";
        context.fillText(""+position, nb_width-2, textLineY);

        //+++++-- Highlight ---
        my_gradient = context.createLinearGradient(x, y, x, y+30);
        my_gradient.addColorStop(0, "rgba(255,255,255,0.0)");
        my_gradient.addColorStop(1, "rgba(255,255,255,0.3)");
        context.fillStyle = my_gradient;
        roundRect(context,x+7, y+8, nb_width-12, 15,10,true,false);
        context.restore();

        context.save();

        var my_gradient = context.createLinearGradient(x, y, x, y+20);
        my_gradient.addColorStop(0, "rgba(255,255,255,0.8)");
        my_gradient.addColorStop(1, "rgba(255,255,255,0.0)");
        context.fillStyle = my_gradient;
        var endCompetitorGradientX =    middle - vs_width;

        roundRect(context, x+nb_width+10, y+5 , endCompetitorGradientX, 40, 20, true, false);




        //-------- Competitor1 -----------
        var flag1_width = flag_width;
        if (!row.competitor1.country){
            flag1_width = 0;
        }


        //Competitor name
        context.fillStyle = "#333";
        context.textAlign = "left";
        context.font = fontStyle;
        context.fillText(row.competitor1.name, x+nb_width+flag1_width+flag_width_margin, textLineY);

        //Competitor warnings
        if ( row.competitor1.warnings > 0 ) {
            var cardImage = new Image();
            cardImage.src = getWarningsImageSrc(row.competitor1);

            cardImage.onload = function() {
                context.save();
                context.shadowOffsetX = 3;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 5;
                var ratio = this.width / this.height;
                var ih = rowHeight - 17;
                var iw = ih * ratio;
                context.drawImage(cardImage, middle-vs_width - 100 - 80, y+6, rowHeight-17, rowHeight-17);

                if (row.competitor1.warnings == 2)
                {
                    context.drawImage(cardImage, middle - vs_width - 100 - 80 - iw + 12 , y+6, rowHeight-17, rowHeight-17);
                }

                context.restore();
            };
        }

        // Competitor country flag ------------
        if ( row.competitor1.country ) {
            var flag = new Image();
            flag.src = getCountryImageSrc(row.competitor1.country);
            flag.onload = function() {
                context.save();
                context.shadowOffsetX = 3;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 5;
                //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                context.drawImage(flag,x+nb_width+flag_width_margin,y+6,rowHeight-17,rowHeight-17);

                context.restore();
            };
        }

        // Competitor mark
        if (row.competitor1.mark){
            context.save();
            var markText = (row.competitor1.mark < 0)?'DSQ':row.competitor1.mark;
            context.fillText(markText, middle-vs_width - 30 - 80, textLineY);   // TODO: create getClassifiedImageWidth

            context.restore();
            //+++++-- Highlight ---
//            my_gradient = context.createLinearGradient(x, y, x, y+30);
//            my_gradient.addColorStop(0, "rgba(255,255,255,0.0)");
//            my_gradient.addColorStop(1, "rgba(255,255,255,0.4)");
//            context.fillStyle = my_gradient;
//            roundRect(context,x+middle - vs_width - mark_width-3, y+8, mark_width-6, 15,10,true,false);

        }

//        //draw classified status
        if (row.competitor1.qualifyStatus != null || row.competitor1.classified != null){
            var classifiedImage = new Image();
            classifiedImage.src = (row.competitor1.qualifyStatus != null)?getQualifyStatusImageSrc(row.competitor1.qualifyStatus):getClassifiedStatusImageSrc(row.competitor1.classified);
            classifiedImage.onload = function() {
                var posX = middle - 80; // TODO: create getClassifiedImageWidth
                var posY = y + 7;
                var ratio = this.width / this.height;
                var ih = rowHeight - 17;
                var iw = ih * ratio;
                context.save();
                context.shadowOffsetX = 1;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 7;
                context.drawImage(this,posX,posY,iw,ih);

                context.restore();
            };
        }



        //get middle

        //draw VS image

        var vs = new Image();
        vs.src = "images/icons/vs_1.png";
        vs.onload = function() {
            var posX = middle;
            var posY = y + 10;
            var ratio = this.width / this.height;
            var ih = rowHeight - 17;
            var iw = ih * ratio;
            vs_width = iw;
            context.save();
            context.shadowOffsetX = 1;
            context.shadowOffsetY = 3;
            context.shadowColor = "rgba(0,0,0,0.3)";
            context.shadowBlur = 7;
            context.drawImage(this,posX,posY,iw,ih);

            context.restore();
        };

        //-------- Competitor2 -----------

        context.save();

        var my_gradient = context.createLinearGradient(x, y, x, y+20);
        my_gradient.addColorStop(0, "rgba(255,255,255,0.8)");
        my_gradient.addColorStop(1, "rgba(255,255,255,0.0)");
        context.fillStyle = my_gradient;
        var endCompetitorGradientX =    middle - vs_width/2;

        roundRect(context, middle + vs_width +10, y+5 , endCompetitorGradientX, 40, 20, true, false);

        //Competitor name
        var flag2_width = flag_width;
        if (!row.competitor2.country){
            flag2_width = 0;
        }

        context.fillStyle = "#333";
        context.textAlign = "left";
        context.font = fontStyle;
        context.fillText(row.competitor2.name, x + middle + vs_width + flag2_width + 2*flag_width_margin , textLineY);

        //Competitor warnings
        if ( row.competitor2.warnings > 0 ) {
            var cardImage = new Image();
            cardImage.src = getWarningsImageSrc(row.competitor2);

            cardImage.onload = function() {
                context.save();
                context.shadowOffsetX = 3;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 5;
                var ratio = this.width / this.height;
                var ih = rowHeight - 17;
                var iw = ih * ratio;
                context.drawImage(cardImage, w - vs_width - 100 - 80, y+6, rowHeight-17, rowHeight-17);

                if (row.competitor2.warnings == 2)
                {
                    context.drawImage(cardImage,w - vs_width - 100 - 80 - iw + 12  , y+6, rowHeight-17, rowHeight-17);
                }

                context.restore();
            };
        }

        // Competitor country flag ------------
        if ( row.competitor2.country ) {
            var flag2 = new Image();
            flag2.src = getCountryImageSrc(row.competitor2.country);
            flag2.onload = function() {
                context.save();
                context.shadowOffsetX = 3;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 5;
                //context.drawImage(flag,x+w+posX+3,y+3,rowHeight-17,rowHeight-17);
                context.drawImage(flag2, middle + vs_width + 2* flag_width_margin,y+6,rowHeight-17,rowHeight-17);

                context.restore();
            };
        }

        // Competitor mark
        if (row.competitor2.mark){
            context.save();
            var mark2 = (row.competitor2.mark < 0)?'DSQ':row.competitor2.mark;
            context.fillText(mark2, x+w - 100 - 80 - 10, textLineY);   // TODO: create getClassifiedImageWidth

            context.restore();
            //+++++-- Highlight ---
//            my_gradient = context.createLinearGradient(x, y, x, y+30);
//            my_gradient.addColorStop(0, "rgba(255,255,255,0.0)");
//            my_gradient.addColorStop(1, "rgba(255,255,255,0.4)");
//            context.fillStyle = my_gradient;
//            roundRect(context,x+middle - vs_width - mark_width-3, y+8, mark_width-6, 15,10,true,false);

        }

//        //draw classified status
        if (row.competitor2.qualifyStatus != null || row.competitor2.classified != null){
            var classifiedImage = new Image();
            classifiedImage.src = (row.competitor2.qualifyStatus != null)?getQualifyStatusImageSrc(row.competitor2.qualifyStatus):getClassifiedStatusImageSrc(row.competitor2.classified);
            classifiedImage.onload = function() {
                var posX = x+w - 80 -10; // TODO: create getClassifiedImageWidth
                var posY = y + 7;
                var ratio = this.width / this.height;
                var ih = rowHeight - 17;
                var iw = ih * ratio;
                context.save();
                context.shadowOffsetX = 1;
                context.shadowOffsetY = 3;
                context.shadowColor = "rgba(0,0,0,0.3)";
                context.shadowBlur = 7;
                context.drawImage(this,posX,posY,iw,ih);

                context.restore();
            };
        }
    }

	function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
		if (typeof stroke == "undefined") {
			stroke = true;
		}
		if (typeof radius === "undefined") {
			radius = 5;
		}
		ctx.beginPath();
		ctx.moveTo(x + radius, y);
		ctx.lineTo(x + width - radius, y);
		ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
		ctx.lineTo(x + width, y + height - radius);
		ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y+ height);
		ctx.lineTo(x + radius, y + height);
		ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
		ctx.lineTo(x, y + radius);
		ctx.quadraticCurveTo(x, y, x + radius, y);
		ctx.closePath();
		if (stroke) {
			ctx.stroke();
		}
		if (fill) {
			ctx.fill();
		}
	}

	function handleWindowResize() {
		log("handleWindowResize");
		canvas = document.getElementById("epyx_scrolling_table");
		if (!canvas) {
			return;
		}
		canvas.parentNode.style.overflow = "hidden";
		canvas.style.position = "absolute";
		paint(canvas);
		maxHeight = canvas.clientHeight - canvas.parentNode.clientHeight + 10;
		y = offsetWait;
		dy = dyDown;
		toScroll = canvas;
	}

	/**
	 * Scrolling banner
	 */
	return {
		init : function() {
			log("init");
			handleWindowResize();
			window.addEventListener('resize', handleWindowResize, false);
			return setInterval(draw, speed);
		},
        
		startWithContent : function( jsonContent) {
			jsonContent = jsonContent;
			handleWindowResize();
		},
		
		start : function() {
			handleWindowResize();
		}
	};
}();

window.addEventListener('load', epyxMarkTableScroll.init, false);
