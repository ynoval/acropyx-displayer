var intervalsCleaner = function ClearIntervals (){
	function clear(){
		var oldInterval = sessionStorage.getItem("rotateResultInterval");
		if (oldInterval) {
			window.clearInterval(oldInterval);
		}

		var oldRankingInterval = sessionStorage.getItem("rotateRankingInterval");
		if (oldRankingInterval) {
			window.clearInterval(oldRankingInterval);
		}

		var oldTimerTimeout = sessionStorage.getItem("timerTimeout");
		if (oldTimerTimeout) {
			window.clearTimeout(oldTimerTimeout);
		}
	}
	return {
		start: function(){
			clear();
		}
	}
}();

